import React, { useContext, useState, useCallback } from 'react';
import { JSONDataContext } from '../../Contexts/FormDataContext';
import { InputNumber } from '../InputNumber';
import { InputDate } from '../InputDate';
import { InputText } from '../InputText';
import { InputTextarea } from '../InputTextarea';
import { InputCheckbox } from '../InputCheckbox';
import { InputRadioList } from '../InputRadioList';
import { FormField, FormButton, FORM_ACTIONS } from '../../types';

import './generated-form.scss';

export const GeneratedForm = () => {
  const { jsonValue, formData } = useContext(JSONDataContext);
  const [parsedData, setParsedData] = useState(JSON.parse(jsonValue));

  const generateField = (item: FormField) => {
    switch (item.type) {
      case 'number':
        return <InputNumber {...item} />;
      case 'text':
        return <InputText {...item} />;
      case 'date':
        return <InputDate {...item} />;
      case 'textarea':
        return <InputTextarea {...item} />;
      case 'checkbox':
        return <InputCheckbox {...item} />;
      case 'radio':
        return item.values?.length ? <InputRadioList {...item} /> : null;
      default:
        return null;
    }
  };

  const generateFields = useCallback(
    () =>
      parsedData.items.map((item: FormField, index: number) =>
        item.type && item.label ? (
          <div
            className='generated-form__field-wrapper'
            key={`${item.label + index}`}
          >
            {generateField(item)}
          </div>
        ) : null
      ),
    [parsedData]
  );

  const clearForm = useCallback(() => {
    if (formData.size) formData.clear();

    // To clear uncontrolled form fields. Didn't find a better solution.
    // It is possible to try to control dynamic fields using event-delegation by adding special attributes to them
    setParsedData(() => '');
    setTimeout(() => setParsedData(JSON.parse(jsonValue)));
  }, [jsonValue, formData]);

  const trySendForm = () => {
    // Here should be some validation before sending data to server
    if (formData.size) {
      // Here submitting the form with field values from 'formData';
      alert(
        `Data has been sent to the server:\n${Array.from(
          formData.entries()
        ).reduce(
          (acc, item: [string, string]) => acc + `${item[0]}: ${item[1]},\n`,
          ''
        )}`
      );
      // If status=200
      clearForm();
    } else {
      alert('Form is empty');
    }
  };

  const tryClearForm = () => {
    if (confirm('Are you sure you want to clear all fields?')) clearForm();
  };

  const getActionForButton = (action: FORM_ACTIONS) => {
    switch (action) {
      case FORM_ACTIONS.CLEAR:
        return tryClearForm;
      case FORM_ACTIONS.SEND:
        return trySendForm;
      default:
        return null;
    }
  };

  return (
    <>
      <form className='generated-form' onSubmit={(e) => e.preventDefault()}>
        <h2 className='generated-form__title'>
          {parsedData.title || 'Default Title'}
        </h2>
        {parsedData.items?.length ? (
          <div className='generated-form__fields-wrapper'>
            {generateFields()}
          </div>
        ) : null}
        {parsedData.buttons?.length ? (
          <div className='generated-form__buttons-wrapper'>
            {parsedData.buttons.map((item: FormButton) => (
              <button
                className='generated-form__button'
                key={item.text}
                onClick={getActionForButton(item.action)}
              >
                {item.text || 'Empty text'}
              </button>
            ))}
          </div>
        ) : null}
      </form>
    </>
  );
};
