import React, { useContext } from 'react';
import { JSONDataContext } from '../../Contexts/FormDataContext';
import { TABS } from '../../types';

import './tabs.scss';

const tabList = [TABS.CONFIG, TABS.RESULT];

interface TabsProps {
  active: TABS;
  setActiveTab(newActive: TABS): void;
}

export const Tabs: React.FC<TabsProps> = ({ active, setActiveTab }) => {
  const { jsonValidateStatus } = useContext(JSONDataContext);

  const isResultAllowed = jsonValidateStatus === 'valid';
  const toggleTabHandler = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    const elemId = (event.target as HTMLDivElement).id as TABS;
    if (elemId !== TABS.RESULT || isResultAllowed) {
      setActiveTab(elemId);
    }
  };

  const getTabClassName = (item: TABS) => {
    let className = 'tab-list__item';
    if (item === 'result' && !isResultAllowed)
      className += ' tab-list__item--disabled';
    if (active === item) className += ' tab-list__item--active';

    return className;
  };

  return (
    <>
      <div className='tab-list'>
        {tabList.map((item: TABS) => (
          <div
            id={item}
            key={item}
            className={getTabClassName(item)}
            onClick={toggleTabHandler}
          >
            {item[0].toUpperCase() + item.substring(1).toLowerCase()}
            <div className='tab-list__message'>Apply your JSON, please</div>
          </div>
        ))}
      </div>
    </>
  );
};
