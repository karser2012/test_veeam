import React, { useState, useCallback } from 'react';
import { Tabs } from '../Tabs';
import { Configurator } from '../Configutator';
import { GeneratedForm } from '../GeneratedForm';
import { FormDataContext } from '../../Contexts/FormDataContext';

import './constructor-wrapper.scss';

import { TABS } from '../../types';

export const ConstructorWrapper: React.FC = () => {
  const [activeTab, setActiveTab] = useState(TABS.CONFIG);

  const goToResult = useCallback(() => setActiveTab(TABS.RESULT), []);

  return (
    <>
      <h1>Form Constructor</h1>
      <div className='constructor-wrapper'>
        <FormDataContext>
          <Tabs active={activeTab} setActiveTab={setActiveTab} />
          <div className='constructor-wrapper__container'>
            {activeTab === TABS.CONFIG ? (
              <Configurator goToResult={goToResult} />
            ) : (
              <GeneratedForm />
            )}
          </div>
        </FormDataContext>
      </div>
    </>
  );
};
