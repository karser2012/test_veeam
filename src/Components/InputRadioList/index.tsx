import React, { useContext, useCallback, useState } from 'react';

import { JSONDataContext } from '../../Contexts/FormDataContext';
import { InputRadio } from '../InputRadio';
import { FormField } from '../../types';

import './input-radio-list.scss';

export const InputRadioList: React.FC<FormField> = React.memo((item) => {
  const { formData, setFormData } = useContext(JSONDataContext);
  const [fieldValue, setFieldValue] = useState(
    formData.get(item.label) || false
  );

  const onChangeHandler = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setFieldValue(e.target.value);
      setFormData(formData.set(item.label, e.target.value));
    },
    [fieldValue]
  );

  return (
    <div className='input-radio-list'>
      <div className='input-radio-list__title'>{item.label}</div>
      <div className='input-radio-list__wrapper'>
        {item.values.map((value) => (
          <InputRadio
            key={value}
            {...item}
            value={value}
            onChangeHandler={onChangeHandler}
            checked={fieldValue === value}
          />
        ))}
      </div>
    </div>
  );
});
