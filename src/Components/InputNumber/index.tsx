import React, { useState, useContext } from 'react';

import { JSONDataContext } from '../../Contexts/FormDataContext';
import { FormField } from '../../types';
import './input-number.scss';

type Props = {
  min?: number;
  max?: number;
} & Pick<FormField, 'label'>;

export const InputNumber: React.FC<Props> = React.memo(
  ({ label, min = 0, max = 99999999 }) => {
    const { formData, setFormData } = useContext(JSONDataContext);
    const [fieldValue, setFieldValue] = useState(formData?.get(label) || '');

    const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
      setFieldValue(e.target.value);
    };

    const onBlurHandler = () => {
      setFormData(formData.set(label, fieldValue));
    };

    return (
      <div className='input-number'>
        <label htmlFor={label}>{label}</label>
        <input
          id={label}
          name={label}
          type='number'
          min={min}
          max={max}
          onChange={onChangeHandler}
          value={fieldValue as string}
          onBlur={onBlurHandler}
        />
      </div>
    );
  }
);
