import React, { useContext, useState } from 'react';
import { JSONDataContext } from '../../Contexts/FormDataContext';
import { FormField } from '../../types';

import './input-checkbox.scss';

type Props = Pick<FormField, 'label'>;

export const InputCheckbox: React.FC<Props> = React.memo(({ label }) => {
  const { formData, setFormData } = useContext(JSONDataContext);
  const [fieldValue, setFieldValue] = useState(formData.get(label) || false);

  const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFieldValue(e.target.checked);
    setFormData(formData.set(label, e.target.checked));
  };

  return (
    <div className='input-checkbox'>
      <label htmlFor={label}>{label}</label>
      <input
        name={label}
        id={label}
        type='checkbox'
        onChange={onChangeHandler}
        checked={fieldValue as boolean}
      />
    </div>
  );
});
