import React, { useState, useContext } from 'react';

import { JSONDataContext } from '../../Contexts/FormDataContext';
import { FormField } from '../../types';
import './input-textarea.scss';

type Props = {
  min?: number;
  max?: number;
} & Pick<FormField, 'label'>;

export const InputTextarea: React.FC<Props> = React.memo(
  ({ label, min = 0, max = 1000 }) => {
    const { formData, setFormData } = useContext(JSONDataContext);
    const [fieldValue, setFieldValue] = useState(formData.get(label) || '');

    const onChangeHandler = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
      setFieldValue(e.target.value);
    };

    const onBlurHandler = () => {
      setFormData(formData.set(label, fieldValue));
    };

    return (
      <>
        <div className='input-textarea'>
          <label htmlFor={label}>{label}</label>
          <textarea
            name={label}
            id={label}
            cols={15}
            rows={5}
            minLength={min}
            maxLength={max}
            onBlur={onBlurHandler}
            onChange={onChangeHandler}
            value={fieldValue as string}
          ></textarea>
        </div>
      </>
    );
  }
);
