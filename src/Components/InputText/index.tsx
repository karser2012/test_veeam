import React, { useState, useContext } from 'react';

import { JSONDataContext } from '../../Contexts/FormDataContext';
import { FormField } from '../../types';
import './input-text.scss';

type Props = {
  min?: number;
  max?: number;
} & Pick<FormField, 'label'>;

export const InputText: React.FC<Props> = React.memo(
  ({ label, min = 1, max = 1000 }) => {
    const { formData, setFormData } = useContext(JSONDataContext);
    const [fieldValue, setFieldValue] = useState(formData.get(label) || '');

    const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
      setFieldValue(e.target.value);
    };

    const onBlurHandler = () => {
      setFormData(formData.set(label, fieldValue));
    };

    return (
      <div className='input-text'>
        <label htmlFor={label}>{label}</label>
        <input
          name={label}
          id={label}
          type='text'
          minLength={min}
          maxLength={max}
          onBlur={onBlurHandler}
          onChange={onChangeHandler}
          value={fieldValue as string}
        />
      </div>
    );
  }
);
