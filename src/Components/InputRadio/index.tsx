import React from 'react';
import { FormField } from '../../types';

import './input-radio.scss';

type Props = {
  value: string;
  checked: boolean;
  onChangeHandler: (e: React.ChangeEvent<HTMLInputElement>) => void;
} & Pick<FormField, 'label'>;

export const InputRadio: React.FC<Props> = React.memo(
  ({ label, value, checked, onChangeHandler }) => {
    return (
      <div className='input-radio'>
        <input
          name={label}
          id={value}
          type='radio'
          value={value}
          checked={checked}
          onChange={onChangeHandler}
        />
        <label htmlFor={value}>{value}</label>
      </div>
    );
  }
);
