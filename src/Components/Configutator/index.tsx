import React, { useState, useContext } from 'react';
import examples from '../../assets/examples.json';
import { JSONDataContext } from '../../Contexts/FormDataContext';
import { JSON_VALIDATE_STATUS } from '../../types';

import './configurator.scss';

interface Props {
  goToResult: () => void;
}

export const Configurator: React.FC<Props> = ({ goToResult }) => {
  const {
    jsonValue,
    setJsonValue,
    jsonValidateStatus,
    setJsonValidateStatus,
    formData,
  } = useContext(JSONDataContext);
  const [value, setValue] = useState(jsonValue || '');

  const setValueHandler = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    if (jsonValidateStatus !== JSON_VALIDATE_STATUS.DEFAULT)
      setJsonValidateStatus(JSON_VALIDATE_STATUS.DEFAULT);
    if (formData.size) formData.clear();
    setValue(e.target.value);
  };

  const setExampleValue = () => {
    setValue(JSON.stringify(examples, null, 2));
  };

  const checkIsValidJSON = () => {
    try {
      JSON.parse(value);
      setJsonValidateStatus(JSON_VALIDATE_STATUS.VALID);
      setJsonValue(value);

      return true;
    } catch (e) {
      setJsonValidateStatus(JSON_VALIDATE_STATUS.INVALID);

      return false;
    }
  };

  const tryGoToResult = () => {
    if (checkIsValidJSON()) {
      goToResult();
    }
  };

  const getClassNamesForElement = (elementName: string) => {
    let className = `configurator__${elementName}`;
    if (jsonValidateStatus !== 'default')
      className += ` configurator__${elementName}--${jsonValidateStatus}`;

    return className;
  };

  return (
    <div className='configurator'>
      <textarea
        name='configurator'
        id='configurator'
        className={getClassNamesForElement('textarea')}
        cols={30}
        rows={15}
        value={value}
        onChange={setValueHandler}
      />
      <div className='configurator__footer'>
        <button onClick={setExampleValue}>Get demo config</button>
        <button onClick={checkIsValidJSON}>Check</button>
        <button onClick={tryGoToResult}>Apply</button>
        {jsonValidateStatus !== 'default' && (
          <div className={getClassNamesForElement('message')}>
            {jsonValidateStatus === 'invalid'
              ? 'Your JSON is invalid. Check it please'
              : 'Your JSON is valid!:) You can go to the results'}
          </div>
        )}
      </div>
    </div>
  );
};
