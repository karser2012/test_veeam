import React from 'react';

import { ConstructorWrapper } from '../ConstructorWrapper';

import './app.scss';

export const App: React.FC = () => (
  <div className='app-container'>
    <ConstructorWrapper />
  </div>
);
