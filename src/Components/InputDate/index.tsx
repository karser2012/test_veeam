import React, { useState, useContext } from 'react';

import { JSONDataContext } from '../../Contexts/FormDataContext';
import { FormField } from '../../types';
import './input-date.scss';

type Props = {
  min?: string;
  max?: string;
} & Pick<FormField, 'label'>;

export const InputDate: React.FC<Props> = React.memo(
  ({ label, min = '1970-01-01', max = '2030-12-31' }) => {
    const { formData, setFormData } = useContext(JSONDataContext);
    const [fieldValue, setFieldValue] = useState(formData.get(label) || '');

    const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
      setFieldValue(e.target.value);
    };

    const onBlurHandler = () => {
      setFormData(formData.set(label, fieldValue));
    };

    return (
      <div className='input-date'>
        <label htmlFor={label}>{label}</label>
        <input
          name={label}
          id={label}
          type='date'
          min={min}
          max={max}
          onBlur={onBlurHandler}
          onChange={onChangeHandler}
          value={fieldValue as string}
        />
      </div>
    );
  }
);
