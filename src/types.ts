export enum TABS {
  CONFIG = 'config',
  RESULT = 'result',
}

export enum JSON_VALIDATE_STATUS {
    DEFAULT = 'default',
    INVALID = 'invalid',
    VALID = 'valid',
}

type FieldType = 'number' | 'text' | 'textarea' | 'checkbox' | 'date' | 'radio';

export type FormField = {
  label: string,
  type: FieldType;
  values?: string[]
}

export type FormButton = {
  text: string,
  action?: FORM_ACTIONS,
}

export enum FORM_ACTIONS {
  SEND = 'send',
  CLEAR = 'clear',
}

export type FieldValue = string | boolean;
