import React, { useState } from 'react';
import { JSON_VALIDATE_STATUS, FieldValue } from '../../types';

type ContextValues = {
  jsonValue?: string;
  setJsonValue?: (json: string) => void;
  setJsonValidateStatus?: (status: JSON_VALIDATE_STATUS) => void;
  jsonValidateStatus?: JSON_VALIDATE_STATUS;
  formData?: Map<string, FieldValue>;
  setFormData?: (map: Map<string, FieldValue>) => void;
};

export const JSONDataContext: React.Context<ContextValues> =
  React.createContext({});

export const FormDataContext: React.FC<React.PropsWithChildren<{}>> = ({
  children,
}) => {
  const [jsonValue, setJsonValue] = useState('');
  const [jsonValidateStatus, setJsonValidateStatus] = useState(
    JSON_VALIDATE_STATUS.DEFAULT
  );
  const [formData, setFormData] = useState(new Map());

  return (
    <JSONDataContext.Provider
      value={{
        jsonValue,
        setJsonValue,
        jsonValidateStatus,
        setJsonValidateStatus,
        formData,
        setFormData,
      }}
    >
      {children}
    </JSONDataContext.Provider>
  );
};
