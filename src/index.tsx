import './styles/styles';
import * as ReactDOM from 'react-dom/client';
import { App } from './Components/App';

const root = ReactDOM.createRoot(document.getElementById('app'));
root.render(App({}));
